import sqlite3
import datetime

db_connection = sqlite3.connect("log_results.db")


def read_file(filename):
    f = open(filename, "r")
    line_count = 0
    for line in f.readlines():
        line_count += 1
        if line_count == 1:
            continue
        else:
            data = line.split()
            if not len(data) > 0:
                continue
            request_id = data[0]
            timestamp = int(data[1])
            exception_type = data[2]
            query = """INSERT INTO log_entries (request_id, timestamp, exception_type)
                VALUES ('%s', %d, '%s');""" % (request_id, timestamp, exception_type)
            db_connection.execute(query)
    db_connection.commit()


def read_files(file_names, concurrency_limit=5):
    # replace this with async code
    while file_names:
        if len(file_names) > concurrency_limit:
            files_to_be_processed = file_names[:concurrency_limit]
            file_names = file_names[concurrency_limit:]
        else:
            files_to_be_processed = file_names
            file_names = []
        for file_name in files_to_be_processed:
            read_file(file_name)


def create_table_if_does_not_exist():
    """Create the table if it does not exist"""
    query = """CREATE TABLE IF NOT EXISTS log_entries(
        ID INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,
        request_id TEXT,
        timestamp INTEGER,
        exception_type CHAR(100));"""
    db_connection.execute(query)


def get_list_of_files_to_read():
    name_of_files = []
    while True:
        filename = input("Enter log file name: ")
        if filename == "":
            break
        name_of_files.append(filename)
    return name_of_files


def get_concurrency_limit():
    try:
        limit = int(input("Enter concurrency limit: "))
    except ValueError:
        print("Invalid Concurrency limit. Please choose again")
        return get_concurrency_limit()
    return limit


if __name__ == "__main__":
    create_table_if_does_not_exist()
    files_list = get_list_of_files_to_read()
    concurrency_limit = get_concurrency_limit()
    read_files(files_list)
